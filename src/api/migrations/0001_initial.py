# Generated by Django 2.0.6 on 2018-06-19 12:14

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Movie',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('director', models.TextField()),
                ('genre', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(blank=True, max_length=100), size=10)),
                ('imdb_score', models.FloatField(default=0.0)),
                ('popularity', models.FloatField(default=0.0)),
            ],
        ),
    ]
