from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from rest_framework import generics, permissions
from rest_framework.filters import SearchFilter
from .serializers import MovieSerializer
from .models import Movie


class CreateView(generics.ListCreateAPIView):
    queryset = Movie.objects.all()
    paginate_by = 10
    serializer_class = MovieSerializer
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly, )
    filter_backends = (SearchFilter, )
    search_fields = ('name', 'director')

    def post(self, request, *args, **kwargs):
        if type(request.data) == list:
            for obj_data in request.data:
                new_obj = Movie(**obj_data)
                new_obj.save()
            return self.list(request, *args, **kwargs)
        else:
            return self.create(request, *args, **kwargs)


class DetailsView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly, )
