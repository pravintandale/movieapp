from django.db import models
from django.contrib.postgres.fields import ArrayField
#from jsonfield import JSONField
import decimal


class Movie(models.Model):
    name = models.TextField()
    director = models.TextField()
    genre = ArrayField(
        models.CharField(max_length=100, blank=True),
        size=10)
    imdb_score = models.FloatField(default=0.0)
    popularity = models.FloatField(default=0.0)
