from rest_framework.test import APIClient, APITestCase
from requests.auth import HTTPBasicAuth
from rest_framework import status
from django.test import TestCase
from django.urls import reverse
from .models import Movie
import requests


class MovieTestCase(TestCase):

    def setUp(self):
        self.movie_name = "Test Movie"
        self.director = "Pravin"
        self.genre = ["Adventure", "Musical"]
        self.imdb_score = 8.0
        self.popularity = 90.0
        self.movie = Movie(name=self.movie_name, director=self.director,
                           genre=self.genre, imdb_score=self.imdb_score,
                           popularity=self.popularity)

    def test_model_sould_create_movie(self):
        count_before = Movie.objects.count()
        self.movie.save()
        count_after = Movie.objects.count()
        self.assertNotEqual(count_before, count_after)


class ViewTestCase(APITestCase):
    def setUp(self):
        self.movie_data = {'name': 'test1', 'director': 'pravin',
                           'genre': ["Adventure", "Musical"], 'imdb_score': 5.0,
                           'popularity': 60}

    def test_api_should_create_movie(self):
        response = requests.post(
            "http://127.0.0.1:8000/movie/",
            data=self.movie_data,
            auth=HTTPBasicAuth('pp', 'Pravin@12'))
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_api_should_update_movie(self):
        response = requests.post(
            "http://127.0.0.1:8000/movie/",
            data=self.movie_data,
            auth=HTTPBasicAuth('pp', 'Pravin@12'))
        index = response.json()['id']

        response = requests.patch(
            "http://127.0.0.1:8000/movie/" + str(index) + "/",
            data=self.movie_data,
            auth=HTTPBasicAuth('pp', 'Pravin@12')
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_api_should_delete_movie(self):
        response = requests.post(
            "http://127.0.0.1:8000/movie/",
            data=self.movie_data,
            auth=HTTPBasicAuth('pp', 'Pravin@12'))
        index = response.json()['id']

        response = requests.delete(
            "http://127.0.0.1:8000/movie/" + str(index) + "/",
            auth=HTTPBasicAuth('pp', 'Pravin@12')
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_api_should_not_create_move_if_not_admin(self):
        response = requests.post(
            "http://127.0.0.1:8000/movie/",
            data=self.movie_data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_api_should_not_update_move_if_not_admin(self):
        response = requests.get(
            "http://127.0.0.1:8000/movie/",
            data=self.movie_data)

        index = response.json()['results'][0]['id']

        response = requests.patch(
            "http://127.0.0.1:8000/movie/" + str(index) + "/",
            data=self.movie_data)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_api_should_not_delete_move_if_not_admin(self):
        response = requests.get(
            "http://127.0.0.1:8000/movie/",
            data=self.movie_data)

        index = response.json()['results'][0]['id']

        response = requests.delete(
            "http://127.0.0.1:8000/movie/" + str(index) + "/",
            data=self.movie_data)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_api_should_not_delete_move_if_not_admin_but_auth(self):
        response = requests.get(
            "http://127.0.0.1:8000/movie/",
            data=self.movie_data)

        index = response.json()['results'][0]['id']

        response = requests.delete(
            "http://127.0.0.1:8000/movie/" + str(index) + "/",
            data=self.movie_data,
            auth=HTTPBasicAuth('ppp', 'notadmin')
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_api_should_not_create_move_if_not_admin_but_auth(self):
        response = requests.post(
            "http://127.0.0.1:8000/movie/",
            data=self.movie_data,
            auth=HTTPBasicAuth('ppp', 'notadmin')
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_api_should_not_update_move_if_not_admin_but_auth(self):
        response = requests.get(
            "http://127.0.0.1:8000/movie/",
            data=self.movie_data)

        index = response.json()['results'][0]['id']

        response = requests.patch(
            "http://127.0.0.1:8000/movie/" + str(index) + "/",
            data=self.movie_data,
            auth=HTTPBasicAuth('ppp', 'notadmin')
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
