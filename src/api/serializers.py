from rest_framework import serializers
from .models import Movie


class MovieSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = Movie
        fields = ('id', 'name', 'director', 'genre',
                  'imdb_score', 'popularity')
